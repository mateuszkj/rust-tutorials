use std::mem;

fn main() {
    let mut s2 = format!("masło maślane");
    s2.push('🦆');
    println!("s2: {s2}");

    // let mut s1: Option<String> = None;
    // s1.push('🦆');
    // print_string_option_by_ref(&s1);

    // s1 = Some(s2);
    // print_string_option_by_ref(&s1);

    // println!("sizeof String = {}", mem::size_of::<String>());
    // println!("sizeof Option<String> = {}", mem::size_of::<Option<String>>());

    // assert_eq!(mem::size_of::<String>(), mem::size_of::<Option<String>>());

    // print_string_option_by_internal_ref(s1.as_ref());
}

fn print_string_option_by_ref(s: &Option<String>) {
    match s {
        Some(str) => println!("string by ref: {str}"),
        None => println!("string by ref: None"),
    }
}

fn print_string_option_by_internal_ref(s: Option<&String>) {
    match s {
        Some(str) => println!("string by ref: {str}"),
        None => println!("string by ref: None"),
    }
}
