use std::mem;

fn main() {
    let s = "masło maślane".to_string();
    println!(
        "s: {s:?} len: {} cap: {}, ptr: {:p}",
        s.len(),
        s.capacity(),
        s.as_ptr()
    );

    let boxed_str = s.into_boxed_str();
    println!(
        "boxed_str: {boxed_str:?}, len: {}. ptr: {:p}",
        boxed_str.len(),
        boxed_str.as_ptr()
    );
    print_str(&boxed_str);

    println!("sizeof String = {}", mem::size_of::<String>());
    println!("sizeof Box<str> = {}", mem::size_of::<Box<str>>());
}

fn print_str(str: &str) {
    println!("print str: {str}");
}
