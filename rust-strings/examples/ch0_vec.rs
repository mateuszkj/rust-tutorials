fn print_list_by_ref(list: &Vec<u8>) {
    println!(
        "list ref: {list:?} has {} elements, capacity: {}",
        list.len(),
        list.capacity()
    );
}

fn print_list_by_slice(slice: &[u8]) {
    println!("list slice: {slice:?} has {} elements", slice.len());
}

fn main() {
    let mut arr = Vec::new();
    print_list_by_ref(&arr);

    arr.push(1u8);
    arr.push(2);
    arr.push(3);

    print_list_by_ref(&arr);
    print_list_by_slice(&arr);

    /*
    let a1 = &arr[1];
    println!("arr[1] = {a1}");
    arr.push(4);
    println!("arr[1] = {a1}");
    */
}
