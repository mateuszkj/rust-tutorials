use std::borrow::Cow;

fn main() {
    let s = "Masło Maślane".to_string();

    println!("upper: {}", to_uppercase_better_than_std(&s));
}

fn to_uppercase_better_than_std(s: &str) -> Cow<str> {
    if s.chars().all(|c| c.is_uppercase()) {
        Cow::Borrowed(s)
    } else {
        Cow::Owned(s.to_uppercase())
    }
}
