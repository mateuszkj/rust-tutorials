use heapless::String;

fn main() {
    let mut s1: String<10> = String::new();

    let ok = s1.push_str("masło").is_ok();
    println!(
        "s1: {s1:?} ok: {ok} len: {} cap: {}",
        s1.len(),
        s1.capacity()
    );

    let ok = s1.push_str(" maślane").is_ok();
    println!(
        "s1: {s1:?} ok: {ok} len: {} cap: {}",
        s1.len(),
        s1.capacity()
    );
}
