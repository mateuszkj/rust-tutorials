use std::ffi::{c_char, CStr, CString};
use std::mem;

#[allow(non_camel_case_types)]
pub type size_t = usize;

extern "C" {
    pub fn strlen(cs: *const c_char) -> size_t;
}

fn main() {
    let s1 = String::from("masło maślane");
    println!(
        "My string {s1:?} has length {} bytes and {} utf-8 characters, address: {:p}",
        s1.len(),
        s1.chars().count(),
        &s1
    );

    // BAD
    // let len = unsafe { strlen(&s1) };
    // println!("strlen len = {len}");

    // VERY BAD
    // let first_byte_ptr = s1.as_ptr(); // s1.as_bytes().as_ptr();
    // unsafe { print_c_string_ptr(first_byte_ptr as *const c_char) };

    // OK
    // let s2 = CString::new(s1).unwrap();
    // println!("s2: {s2:?}, address: {:p}", &s2);
    // unsafe { print_c_string_ptr(s2.as_ptr()) };

    // unsafe { print_c_string_by_ref(&s2) };

    // println!("sizeof String = {}", mem::size_of::<String>());
    // println!("sizeof &str = {}", mem::size_of::<&str>());

    // println!("sizeof CString = {}", mem::size_of::<CString>());
    // println!("sizeof &CStr = {}", mem::size_of::<&CStr>());
    // println!("sizeof *const c_char = {}", mem::size_of::<*const c_char>());

    // println!("sizeof str = {}", mem::size_of::<str>());
    // println!("sizeof CStr = {}", mem::size_of::<CStr>());
}

unsafe fn print_c_string_ptr(str: *const c_char) {
    let len = unsafe { strlen(str) };
    println!("strlen len = {len}, address = {str:p}");
    unsafe { libc::puts(str) };
}

unsafe fn print_c_string_by_ref(cstr: &CStr) {
    println!("cstr: {cstr:?} len: {}", cstr.to_bytes().len());
    print_c_string_ptr(cstr.as_ptr());
}
