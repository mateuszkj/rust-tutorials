use smol_str::SmolStr;

fn main() {
    let s1 = SmolStr::new("masło");
    println!(
        "s1: {s1:?} len: {} heap_allocated: {}",
        s1.len(),
        s1.is_heap_allocated()
    );

    let s2 = SmolStr::new("masło maślane");
    println!(
        "s2: {s2:?} len: {} heap_allocated: {}",
        s2.len(),
        s2.is_heap_allocated()
    );

    let s3 = SmolStr::new("A SmolStr is small-string optimized string type with O(1) clone");
    println!(
        "s3: {s3:?} len: {} heap_allocated: {}",
        s3.len(),
        s3.is_heap_allocated()
    );
}
