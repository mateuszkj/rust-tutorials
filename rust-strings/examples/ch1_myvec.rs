use std::alloc::Layout;
use std::ops::{Deref, DerefMut};
use std::{alloc, ptr};

fn print_slice(slice: &[u8]) {
    println!("slice: {slice:?} len: {}", slice.len());
}

fn set_value(slice: &mut [u8], replace: u8) {
    println!("set every value of slice to {replace}");
    slice.into_iter().for_each(|v| *v = replace);
}

fn main() {
    let mut arr = MyVec::new();
    println!("empty: {arr:?}");
    arr.push(1);
    arr.push(2);
    println!("two elements: {arr:?}");

    arr.print_elements();

    print_slice(&arr);
    set_value(&mut arr, 5);
    arr.print_elements();
}

#[derive(Debug)]
pub struct MyVec {
    buf: *mut u8,
    cap: usize,
    len: usize,
}

impl MyVec {
    pub fn new() -> MyVec {
        Self {
            buf: ptr::null_mut(),
            cap: 0,
            len: 0,
        }
    }

    pub fn print_elements(&self) {
        for i in 0..self.cap {
            let addr = unsafe { self.buf.add(i) };
            let value = unsafe { *addr };
            println!("[{i}] = {value} at address {addr:p}");
        }
    }

    pub fn push(&mut self, element: u8) {
        if self.len == self.cap {
            self.resize();
        }
        assert!(self.cap > self.len);

        unsafe {
            let new_element_ptr = self.buf.add(self.len);
            *new_element_ptr = element
        }
        self.len += 1;
    }

    fn resize(&mut self) {
        let new_cap = if self.cap == 0 { 8 } else { self.cap * 2 };

        let new_layout = Layout::array::<u8>(new_cap).expect("Bad layout");

        let new_buf = if self.buf.is_null() {
            unsafe { alloc::alloc(new_layout) }
        } else {
            let old_layout = Layout::array::<u8>(self.cap).unwrap();
            unsafe { alloc::realloc(self.buf, old_layout, new_layout.size()) }
        };

        assert!(!new_buf.is_null());

        self.buf = new_buf;
        self.cap = new_cap;
    }
}

impl Drop for MyVec {
    fn drop(&mut self) {
        if !self.buf.is_null() {
            let layout = Layout::array::<u8>(self.cap).unwrap();
            unsafe {
                alloc::dealloc(self.buf, layout);
            }
        }
    }
}

impl Deref for MyVec {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe { std::slice::from_raw_parts(self.buf, self.len) }
    }
}

impl DerefMut for MyVec {
    fn deref_mut(&mut self) -> &mut [u8] {
        unsafe { std::slice::from_raw_parts_mut(self.buf, self.len) }
    }
}
