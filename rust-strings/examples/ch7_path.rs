use std::env;
use std::ffi::OsString;
use std::path::{Path, PathBuf};

fn main() {
    let home_dir = env::var("HOME").expect("no HOME env");
    println!("home_dir as string = {home_dir}");

    let home_dir = PathBuf::from(home_dir);
    println!(
        "home_dir as PathBuf = {} file_type = {:?}",
        home_dir.display(),
        home_dir.metadata()
    );
    list_files(&home_dir);
}

fn list_files(path: &Path) {
    for e in std::fs::read_dir(path).unwrap() {
        let e = e.unwrap();

        let file_name: OsString = e.file_name();
        let file_name_str = file_name.to_str().unwrap();

        println!("{}", file_name_str.to_uppercase());
    }
}
