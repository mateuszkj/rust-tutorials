fn print_string_by_ref(s: &String) {
    println!(
        "text by ref: \"{s}\" addr: {:p} len: {} capacity: {}",
        s.as_ptr(),
        s.len(),
        s.capacity()
    );
}

fn print_string_by_slice(s: &str) {
    println!(
        "text by slice: \"{s}\" addr: {:p} len: {}",
        s.as_ptr(),
        s.len()
    );
}

fn main() {
    /*
    pub struct String {
        vec: Vec<u8>,
    }
    */

    let mut s1 = String::new();
    print_string_by_ref(&s1);
    print_string_by_slice(&s1);

    // s1.push_str("masło maślane");
    // print_string_by_ref(&s1);
    // print_string_by_slice(&s1);

    // s1.push('!');
    // print_string_by_ref(&s1);
    // print_string_by_slice(&s1);

    // let word = &s1[0..6]; // [0..6] [..6] [7..]
    // print_string_by_slice(word);

    // let word = &s1[0..4];
    // print_string_by_slice(word);

    // for (idx, c) in s1.chars().enumerate() {
    //     println!("char[{idx}] = {c} len: {}", c.len_utf8());
    // }
}
