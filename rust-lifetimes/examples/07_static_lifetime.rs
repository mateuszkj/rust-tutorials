fn text_for_error(error: u8) -> &str {
    match error {
        0 => "abc",
        1 => "cd",
        _ => "other",
    }
}

fn main() {
    println!("error = {}", text_for_error(1));
}
