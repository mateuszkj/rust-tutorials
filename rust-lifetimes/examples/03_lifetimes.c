#include <stdio.h>
#include <string.h>

const char *longer_str(const char *s1, const char *s2) {
    if (strlen(s1) > strlen(s2)) {
        return s1;
    } else {
        return s2;
    }
}

const char *fun1() {
    const char *str1 = "Hello";
    char str2[128];
    strcpy(str2, "Hello Word");
    return longer_str(str1, str2);
}

int main() {
    const char *longer = fun1();
    printf("Longer string: %s\n", longer);
}