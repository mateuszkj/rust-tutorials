fn longer_str(s1: &str, s2: &str) -> &str {
    if s1.len() > s2.len() {
        s1
    } else {
        s2
    }
}

fn main() {
    let r = longer_str("abc", "cd");

    println!("r = {r}");
}
