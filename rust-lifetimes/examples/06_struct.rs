struct MyStruct {
    s1: &str,
    s2: &str,
}

impl MyStruct {
    fn longer_str(&self) -> &str {
        if self.s1.len() > self.s2.len() {
            self.s1
        } else {
            self.s2
        }
    }
}

fn main() {
    let r = MyStruct {
        s1: "abc",
        s2: "cd",
    };

    println!("r = {}", r.longer_str());
}
