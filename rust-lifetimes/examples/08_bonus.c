#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int *data;
    size_t size;
    size_t capacity;
} Vector;

void vector_init(Vector *v) {
    v->size = 0;
    v->capacity = 4;
    v->data = malloc(v->capacity * sizeof(int));
}

void vector_push_back(Vector *v, int value) {
    if (v->size == v->capacity) {
        v->capacity *= 2;
        v->data = realloc(v->data, v->capacity * sizeof(int));
    }
    v->data[v->size] = value;
    v->size++;
}

int main() {
    Vector vec;
    vector_init(&vec);

    for (int i = 0; i < 10; ++i) {
        vector_push_back(&vec, i);
    }

    int *ref = &vec.data[2];
    printf("ref_a = %d\n", *ref);

    vector_push_back(&vec, 42);
    printf("ref_b = %d\n", *ref);

    free(vec.data);
    return 0;
}