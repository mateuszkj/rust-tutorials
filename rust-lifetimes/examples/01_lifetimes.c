#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int value;
} MyStruct;

MyStruct *mystruct_new(int value) {
    MyStruct init;
    init.value = value;
    return &init;
}

int main() {
    MyStruct *ptr = mystruct_new(42);

    printf("Value: %d\n", ptr->value);

    return 0;
}
