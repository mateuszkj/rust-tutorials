#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    int value;
} MyStruct;

MyStruct *mystruct_new(int value) {
    MyStruct *init = (MyStruct *)malloc(sizeof(MyStruct));
    assert(init != NULL);

    init->value = value;
    return init;
}

int main() {
    MyStruct *ptr = mystruct_new(42);
    printf("Value: %d\n", ptr->value);

    free(ptr);
    return 0;
}
