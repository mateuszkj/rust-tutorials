fn main() {
    let x;
    {
        // create new scope
        let y = 42;
        x = &y;
    } // y is dropped

    println!("The value of 'x' is {}.", x);
}
